#ifndef _DISPLAY_h
#define _DISPLAY_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include <LedControl.h>
#include "ArduinoInteract.h"



class Display : public ArduinoInteract
{
public: 
	/*initialize the display*/
	void Init();
	
	/*display a wait screen; continues after performed action*/
	void WaitScreen();

	/*displays a loose screen with game end animation*/
	void LooseScreen();

	/*clears the display*/
	void Clear();

	/*turn on/off a led at a certain location*/
	void DisplayLed(int location, bool value);

	/*reset the display*/
	void Reset();

	/*displays the number on the display (valid numbers: 0-64*/
	void DisplayNumbers(int number);

private:
	LedControl led = LedControl(12, 10, 11, 1);
	
	const int delaytime = 250;
	
	int initCounter = 7;

	int position0 = 12;
	int position1 = 9;
	int digit0;
	int digit1;
};



#endif

