#ifndef _SNAKE_h
#define _SNAKE_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "ArduinoInteract.h"

class Snake : public ArduinoInteract
{
public:
	/*initialize the snake*/
	void Init();

	/*reset the snake*/
	void Reset();

	/*spawn an egg*/
	void SpawnEgg();

	/*get the location of the current egg*/
	int GetEggLocation();

	/*get the x coordinate of the snake*/
	int GetSnakePosX();

	/*get the y coordinate of the snake*/
	int GetSnakePosY();

	/*returns the value of the cache Array at the asked position*/
	int GetCache(int position);

	/*returns the cacheCounter variable*/
	int GetCacheCounter();

	/*returns the cacheOffCounter variable*/
	int GetCacheOffCounter();

	/*moves the snake; action should be performed ever game-tick*/
	void Move();

	/*set the recentMove variable*/
	void SetRecentMove(int direction);

	/*returns the reventMove variable*/
	int GetRecentMove();

	/*returns if there is an error (true = yes; false = no)*/
	bool IsError();

	/*returns if the length of the snkae has changed*/
	bool LengthChanged();

	/*get the length of the snake*/
	int GetLength();

private:
	/*properties*/
	int snakePosY;
	int snakePosX;
	int length = 1;
	bool lengthChanged = false;
	bool newSnake = true;

	/*movement*/
	int recentMove = -1;

	/*cache*/
	int cache[64];
	int cacheCounter = 0;
	int cacheOffCounter = 0;

	/*map*/
	int snakeMap[64];

	/*egg*/
	int eggLocation = -1;
	int secureEggSpawn = 0;

	/*others*/
	bool error = false;
	int randomNumber = -1;
};



#endif

