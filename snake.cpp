#include "snake.h"

void Snake::Reset() {
	for (int i = 0; i < 64; i++) {
		snakeMap[i] = 0;
		cache[i] = -1;
	}
	/*properties*/
	snakePosX = 4;
	snakePosY = 4;
	length = 1;
	lengthChanged = false;
	newSnake = true;

	/*movement*/
	recentMove = -1;

	/*cache*/
	cacheCounter = 0;
	cacheOffCounter = 0;

	/*egg*/
	eggLocation = -1;
	secureEggSpawn = 0;

	/*others*/
	error = false;
	randomNumber = -1;
}

void Snake::Init() {
	for (int i = 0; i < 64; i++) {
		snakeMap[i] = 0;
		cache[i] = -1;
	}

	digitalWrite(ledRed, HIGH);
	delay(10);
	digitalWrite(ledRed, LOW);
	delay(100);

	snakePosX = 4;
	snakePosY = 4;
}

int Snake::GetSnakePosX() {
	return snakePosX;
}

int Snake::GetSnakePosY() {
	return snakePosY;
}

void Snake::Move() {
	/*add 'last' location to cache*/
	cache[cacheCounter] = 8 * snakePosY + snakePosX;
	cacheCounter++;
	
	/*add 'last' location to the snake map*/
	snakeMap[8 * snakePosY + snakePosX] = 1;

	if (lengthChanged) {
		lengthChanged = false;
	}
	
	/*movement*/
	if (recentMove == 0) {
		snakePosX = snakePosX - 1;
	}

	if (recentMove == 1) {
		snakePosX = snakePosX + 1;
	}

	if (recentMove == 2) {
		snakePosY = snakePosY + 1;
	}

	if (recentMove == 3) {
		snakePosY = snakePosY - 1;
	}


	/*snake-rules*/
	//if it touches the border
	if (snakePosX > 7 || snakePosY > 7 || snakePosX < 0 || snakePosY < 0) {
		error = true;
	}

	//if it touches itself
	if (snakeMap[8 * snakePosY + snakePosX] == 1) {
		error = true;
	}

	//if it 'eats' an egg
	if (snakePosY * 8 + snakePosX == eggLocation) {
		length++;
		lengthChanged = true;
		eggLocation = -1;
	}

	/*cache*/
	if (cacheCounter % 64 == 0 && cacheCounter != 0) {
		cacheCounter = 0;

	}
	if (cacheOffCounter % 64 == 0 && cacheOffCounter != 0) {
		cacheOffCounter = 0;
	}

	/*delete "spawnpoint" (only in first tick needed)*/
	if (newSnake) {
		snakeMap[cache[cacheOffCounter]] = 0;
		newSnake = false;
	}

	if (lengthChanged == false) {
		cacheOffCounter++;
	}

	/*snake map*/
	snakeMap[cache[cacheOffCounter]] = 0;
}

int Snake::GetRecentMove() {
	return recentMove;
}

void Snake::SetRecentMove(int direction) {
	if (direction == 0) {
		recentMove = 0;
	}
	if (direction == 1) {
		recentMove = 1;
	}
	if (direction == 2) {
		recentMove = 2;
	}
	if (direction == 3) {
		recentMove = 3;
	}
}

void Snake::SpawnEgg() {
	//while loop: the secureEggSpawn int secures the spawnEgg-function from an endless-loop:
	//if (for whatever reason) no location can be found --> the while-loop breaks and "sends 'error-signal'"
	while (secureEggSpawn < 64) {

		//creates a random number between 0 and 63(max value (here:64) is not included!)
		//the seed for the random number is set in the ArduinoInteract.h file
		randomNumber = random(0, 64);

		//check if the proposed egg location is "blocked" by the snake
		if (snakeMap[randomNumber] == 0) {

			//if not (== 0 --> 0 = low): set the random number as the new egg location; break the while-loop
			eggLocation = randomNumber;
			break;
		}
		secureEggSpawn++;
	}
	if (secureEggSpawn == 64) {
		error = true;
	}
}

int Snake::GetEggLocation() {
	return eggLocation;
}

int Snake::GetCacheCounter() {
	return cacheCounter;
}

int Snake::GetCacheOffCounter() {
	return cacheOffCounter;
}

int Snake::GetCache(int position) {
	return cache[position];
}

bool Snake::IsError() {
	if (error) {
		return true;
	}

	return false;
}

bool Snake::LengthChanged() {
	if (lengthChanged) {
		return true;
	}

	return false;
}

int Snake::GetLength() {
	return length;
}
