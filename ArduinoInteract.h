#ifndef _ARDUINOINTERACT_h
#define _ARDUINOINTERACT_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

class ArduinoInteract
{
protected:
	const int ledRed = 4;
	const int analogRandomSeed = 2;
};

#endif

