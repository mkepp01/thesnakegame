#include "game.h"

void Game::Init() {
	//define ledRed as an output
	pinMode(ledRed, OUTPUT);

	//initializing controller with led blinking
	digitalWrite(ledRed, HIGH);
	delay(1000);
	digitalWrite(ledRed, LOW);
	delay(100);
	
	//inizialize controller
	cont->Init();

	//initialize snake
	sna->Init();

	//initialize display
	dis->Init();

	//create random seed
	randomSeed(analogRead(analogRandomSeed));
}

void Game::WaitForStartAction() {
	dis->WaitScreen();
	while (cont->GetRecentAction() == -1) {
		cont->GetAction();
	}
	dis->Clear();
}

void Game::Tick() {
	/*constantly track the actions of the controller*/
	cont->GetAction();

	/*if there is no egg spawned: spawn one and display it*/
	if (sna->GetEggLocation() == -1) {
		sna->SpawnEgg();
		dis->DisplayLed(sna->GetEggLocation(), true);
	}

	/*tick-system: allows to perform certain actions with a delay without using the 'delay-function'*/
	/*the problem with the 'delay-function' is that it "pauses" the game --> really bad because e.g. controller actions need to be tracked*/
	
	/*when the game starts the millis (get by calling the 'millis-function') aren't at zero --> set the tickstart to the current millis if it's zero*/
	if (tickStart == 0) {
		tickStart = millis();
	}

	if (millis() > tickStart + tickTime) {
		tickStart = millis();
		
		if (cont->GetRecentAction() == 0) {
			sna->SetRecentMove(0);
		}
		else {
			if (cont->GetRecentAction() == 1) {
				sna->SetRecentMove(1);
			}
			else {
				if (cont->GetRecentAction() == 2) {
					sna->SetRecentMove(2);
				}
				else {
					if (cont->GetRecentAction() == 3) {
						sna->SetRecentMove(3);
					}
				}
			}
		}
		
		sna->Move();

		/*catch errors*/
		if (sna->IsError()) {
			Reset();
			WaitForStartAction();
			return;
		}
		
		/*display snake*/
		/*display the "new snakepoint"*/
		dis->DisplayLed(8 * sna->GetSnakePosY() + sna->GetSnakePosX(), true);

		/*"delete" the "oldest snakepoint" (in accordance with the snake length)*/
		dis->DisplayLed(sna->GetCache(sna->GetCacheOffCounter()), false);
	}	
}

void Game::Reset() {
	/*loose animation*/
	dis->LooseScreen();
	dis->Clear();
	dis->DisplayNumbers(sna->GetLength());
	delay(2000);

	/*own variables to reset*/
	tickStart = 0;

	/*other instances to reset*/
	dis->Reset();
	sna->Reset();
	cont->Reset();
}







