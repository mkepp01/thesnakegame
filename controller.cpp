#include "controller.h"

/// DEFINE ///
#define X analogRead(X_pin)
#define Y analogRead(Y_pin)
/// DEFINE ///

void Controller::Init() {
	digitalWrite(ledRed, HIGH);
	delay(100);
	digitalWrite(ledRed, LOW);
    delay(100);
}

void Controller::GetAction() {
    if (X > 1000 && Y < 600 && Y > 400) {
        recentAction = 0;
    }
    if (X < 300 && Y < 600 && Y > 400) {
        recentAction = 1;
    }
    if (Y > 600 && X > 300 && X < 1000) {
        recentAction = 2;
    }
    if (Y < 400 && X > 300 && X < 1000) {
        recentAction = 3;
    }
}

int Controller::GetRecentAction() {
    return recentAction;
}

void Controller::Reset() {
    recentAction = -1;
}