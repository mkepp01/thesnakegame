#ifndef _CONTROLLER_h
#define _CONTROLLER_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#include "ArduinoInteract.h"

class Controller : public ArduinoInteract
{
public:
	/*initialize the controller*/
	void Init();

	/*saves the current action to the recentAction variable*/
	/*0 = left, 1 = right, 2 = up, 3 = down, -1 = error*/
	void GetAction();

	/*returns the value of the recentAction variable*/
	int GetRecentAction();

	/*reset the controller*/
	void Reset();

private:
	//analog pin 0 connected to X_pin
	const int X_pin = 0;
	//analog pin 1 connected to Y_pin
	const int Y_pin = 1;

	int recentAction = -1;
};



#endif

