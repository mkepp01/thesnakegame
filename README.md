# TheSnakeGame
This is the SnakeGame for arduino.

Here are some things about the project: <br />

## Paradigm
Comming soon...
## How does it work?
### Controller
The controller is a pretty simple component. I am currently using four (of five) ports of the controller (the not used one allows it to track wheter the joystick is "pressed"). Two of the connections are for power (5v; GND). The other ones are plugged in the analog ports and transmit the information for the X and Y position of the joystick.

#### Important functions:
- The `Init()` function initializes the controller. It is automatically called when initializing the game. In this certain case initializing the controller basically means to light up the led before the game is started. By doing so you can easily check if the the controller is implemented the right way.

- The `GetAction()` function mainly tracks the performed actions of the controller. It is very important to call this function every tick of the game to that you can track most of the performed actions. The function reads the analog pins using the `analogRead(...)` function and returns a certain value which contains informations about the position of the joystick. So you can create the four needed "movement directions" by checking if the controller is in a certain position and (if that is the case) then save this as an action. These defined actions are saved in the integer `recentActions`. This has several advantages but mainly it allows us to call the `GetAction()` function ever game tick and only save "valid" actions. So the snake moves in latest tracked direction and ignores all "non-valid-actions"like when the joystick isn't moved.

### Display
The display is represented by 64 leds (eight rows, each with eight columns). The positions on the display / "map" are given in two different ways: <br />
|   | 7x | 6x | 5x | 4x | 3x | 2x | 1x | 0x |
|---|----|----|----|----|----|----|----|----|
| 0y| 7  | 6  | 5  | 4  | 3  | 2  | 1  | 0  |
| 1y| 15 | 14 | 13 | 12 | 11 | 10 | 9  | 8  |
| 2y| 23 | 22 | 21 | 20 | 19 | 18 | 17 | 16 |
| 3y| 31 | 30 | 29 | 28 | 27 | 26 | 25 | 24 |
| 4y| 39 | 38 | 37 | 36 | 35 | 34 | 33 | 32 |
| 5y| 47 | 46 | 45 | 44 | 43 | 42 | 41 | 40 |
| 6y| 55 | 54 | 53 | 52 | 51 | 50 | 49 | 48 |
| 7y| 63 | 62 | 61 | 60 | 59 | 58 | 57 | 56 |
- x and y: Every position on the display can be described with valid x and y values. One advantage of using x and y values is that you can easily interact with the map / display (for example when setting the borders for the snake).

- the absolut position: Every position in the diplay has a unique number (0 - 63). Using the unique numbers to describe positions on the display has the advantage that you can easily work with the positions (only have to handle one instead of two numbers) and for example save them to a cache.

You can easily switch between both methods:
- Position: 8 * y + x
- Y: Position / 8 (int) | X: Position % 8

#### Imporant functions
- The `Init()` function initializes the display. It is automatically called when initializing the game. In this certain case initializing the display basically means that a small "animation" is played on the display. By doing that you can see wheter the display displays things as expected and if all leds are working correctly.

- The `DisplayLed(int location, bool value)` function can turn a led (at the given location) on or off (bool value).

### Snake
The snake class has (among other things) two integers (`snakePosX` and `snakePosY`) which represent the current "head" position of the snake. The constructor (and the `Reset()` function) sets these two integers `= 4` by default. The snake isn't displayed as an "whole object" meaning that it's not like every game tick the "old snake" is deleted and the "new snake" is displayed. It's displayed by always turning on the led of the current game tick and turning off the led in the cache (which position in the cache is chosen depends on the length of the snake). This technique allows it to check if the snake crashes with the border. But when it comes to check wheter the snake touches it own tail it starts to get problematic since there is no (known) method to check which leds are currently `HIGH` on the display. That's why the snake class also has the `snakeMap[64]` array. Each of the 64 elements in the array (obviously) represents a position on the map (for more information check the "Display" section). The values of all elements (default: 0) can be set to 0 (`LOW` --> led is off) or 1 (`HIGH` --> led is on). Every position of the "snake head" is also added to the `snakeMap[]` array (: `snakeMap[location of the "snake head"] = 1`) and the other way around is set to `LOW` and the position of the snake cache.

#### Important functions
Besides the `Init()` function there is no "one important" function you need to know. Most of them can only be to be used under certain conditions. You can look the functions up in the `snake.h` file and where they are used. Normally, each function should have a little description where it is declared.

