#include "ArduinoInteract.h"
#include "snake.h"
#include "game.h"

Game game;

void setup() {
    game.Init();
    game.WaitForStartAction();
}

void loop() {
    game.Tick();
}

