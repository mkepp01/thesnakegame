#include "display.h"

/*numbers: start*/
int zero[] = { 0,1,2,8,10,16,18,24,26,32,33,34 };
int one[] = { 1,10,9,17,25,33 };
int two[] = { 1,10,8,17,26,34,33,32 };
int three[] = { 2,1,0,8,16,17,18,24,32,33,34 };
int four[] = { 2,10,18,17,16,25,33 };
int five[] = { 0,1,2,10,18,17,16,24,32,33,34 };
int six[] = { 0,1,2,10,18,17,16,26,24,32,33,34 };
int seven[] = { 0,1,2,10,8,16,24,32 };
int eight[] = { 0,1,2,10,8,18,17,16,26,24,34,33,32 };
int nine[] = { 0,1,2,10,8,18,17,16,24,32 };
/*numbers: end*/

void Display::Init() {
	led.shutdown(0, false);
	led.setIntensity(0, 1);
	led.clearDisplay(0);

	for (int i = 0; i < 8; i++) {
		if (i != 0) {
			led.setLed(0, 0, i, true);
			led.setLed(0, 0, i - 1, false);
			led.setLed(0, i, 0, true);
			led.setLed(0, i - 1, 0, false);

			led.setLed(0, 7, initCounter, true);
			led.setLed(0, 7, initCounter + 1, false);
			led.setLed(0, initCounter, 7, true);
			led.setLed(0, initCounter + 1, 7, false);
		}
		else {
			led.setLed(0, 0, i, true);
			led.setLed(0, initCounter, initCounter, true);
		}
		initCounter--;
		delay(100);
	}
	initCounter = 7;
	for (int i = 0; i < 8; i++) {
		led.setLed(0, 7, initCounter, true);
		led.setLed(0, 0, i, true);

		initCounter--;
		delay(50);
	}
	initCounter = 7;
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 8; j++) {
			led.setLed(0, j, i, HIGH);
			led.setLed(0, j, initCounter, HIGH);
			delay(1);
		}

		initCounter--;
		delay(50);
	}

	led.clearDisplay(0);
}

void Display::WaitScreen() {
	led.clearDisplay(0);
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			led.setLed(0, i, j, true);
			delay(10);
		}
	}
}

void Display::LooseScreen() {
	int randomNumber;
	int cache[64];
	for (int i = 0; i < 64; i++) {
		cache[i] = 0;
	}
	led.setIntensity(0, 15);
	for (int i = 0; i < 64; i++) {
		while (true) {
			randomNumber = random(0, 64);
			if (cache[randomNumber] == 0) {
				cache[randomNumber] = 1;
				led.setLed(0, randomNumber % 8, randomNumber / 8, true);
				break;
			}
		}
		delay(20);
	}
	delay(50);
	led.setIntensity(0, 0);
}

void Display::Clear() {
	led.clearDisplay(0);
}

void Display::Reset() {
	led.clearDisplay(0);
}

void Display::DisplayLed(int location, bool value) {
	if (value) {
		led.setLed(0, location % 8, location / 8, true);
	}
	else if (!value) {
		led.setLed(0, location % 8, location / 8, false);
	}
}

void Display::DisplayNumbers(int number) {
	if (number < 0 || number > 64) {
		return;
	}
	if (number < 10) {
		digit0 = 0;
		digit1 = number;
	}
	else {
		digit0 = (number / 10) % 10;
		digit1 = number % 10;
	}

	switch (digit0) {
	case 0:
		for (int i = 0; i < sizeof(zero) / sizeof(*zero); i++) {
			DisplayLed(zero[i] + position0, true);
		}
		break;
	case 1:
		for (int i = 0; i < sizeof(one) / sizeof(*one); i++) {
			DisplayLed(one[i] + position0, true);
		}
		break;
	case 2:
		for (int i = 0; i < sizeof(two) / sizeof(*two); i++) {
			DisplayLed(two[i] + position0, true);
		}
		break;
	case 3:
		for (int i = 0; i < sizeof(three) / sizeof(*three); i++) {
			DisplayLed(three[i] + position0, true);
		}
		break;
	case 4:
		for (int i = 0; i < sizeof(four) / sizeof(*four); i++) {
			DisplayLed(four[i] + position0, true);
		}
		break;
	case 5:
		for (int i = 0; i < sizeof(five) / sizeof(*five); i++) {
			DisplayLed(five[i] + position0, true);
		}
		break;
	case 6:
		for (int i = 0; i < sizeof(six) / sizeof(*six); i++) {
			DisplayLed(six[i] + position0, true);
		}
		break;
	case 7:
		for (int i = 0; i < sizeof(seven) / sizeof(*seven); i++) {
			DisplayLed(seven[i] + position0, true);
		}
		break;
	case 8:
		for (int i = 0; i < sizeof(eight) / sizeof(*eight); i++) {
			DisplayLed(eight[i] + position0, true);
		}
		break;
	case 9:
		for (int i = 0; i < sizeof(nine) / sizeof(*nine); i++) {
			DisplayLed(nine[i] + position0, true);
		}
		break;
	}
	switch (digit1) {
	case 0:
		for (int i = 0; i < sizeof(zero) / sizeof(*zero); i++) {
			DisplayLed(zero[i] + position1, true);
		}
		break;
	case 1:
		for (int i = 0; i < sizeof(one) / sizeof(*one); i++) {
			DisplayLed(one[i] + position1, true);
		}
		break;
	case 2:
		for (int i = 0; i < sizeof(two) / sizeof(*two); i++) {
			DisplayLed(two[i] + position1, true);
		}
		break;
	case 3:
		for (int i = 0; i < sizeof(three) / sizeof(*three); i++) {
			DisplayLed(three[i] + position1, true);
		}
		break;
	case 4:
		for (int i = 0; i < sizeof(four) / sizeof(*four); i++) {
			DisplayLed(four[i] + position1, true);
		}
		break;
	case 5:
		for (int i = 0; i < sizeof(five) / sizeof(*five); i++) {
			DisplayLed(five[i] + position1, true);
		}
		break;
	case 6:
		for (int i = 0; i < sizeof(six) / sizeof(*six); i++) {
			DisplayLed(six[i] + position1, true);
		}
		break;
	case 7:
		for (int i = 0; i < sizeof(seven) / sizeof(*seven); i++) {
			DisplayLed(seven[i] + position1, true);
		}
		break;
	case 8:
		for (int i = 0; i < sizeof(eight) / sizeof(*eight); i++) {
			DisplayLed(eight[i] + position1, true);
		}
		break;
	case 9:
		for (int i = 0; i < sizeof(nine) / sizeof(*nine); i++) {
			DisplayLed(nine[i] + position1, true);
		}
		break;
	}
}
