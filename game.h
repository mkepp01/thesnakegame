#ifndef _GAME_h
#define _GAME_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "display.h"
#include "controller.h"
#include "snake.h"
#include "ArduinoInteract.h"

class Game : public ArduinoInteract
{
private:
	const int tickTime = 800;
	long tickStart = 0;

protected:
	Display* dis = new Display();
	Controller* cont = new Controller();
	Snake* sna = new Snake();

public:
	/*initialize the game --> automatically initializes the display, snake and controller*/
	void Init();

	/*display a wait screen; continues after performed action*/
	void WaitForStartAction();
	
	/*write everything to this function that should happen in one tick*/
	void Tick();

	/*resets the game --> automatically resets the display, snake and controller*/
	void Reset();
};



#endif

